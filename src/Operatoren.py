"""     Mehrzeiliger Kommentar
#       Einzeiliger Kommentar
+       Addition, Vorzeichen
-       Subtraktion, Vorzeichen
*       Multiplikation
**      Exponentialfunktion         10**3 = 1000
/       Division float              10/3 = 3.3333
//      Division int                10/3 = 3
%       Modulo                      10/3 = 1
<       Kleiner
<=      Kleiner gleich
>       Grösser
>=      Grösser gleich
!=      Nicht gleich
==      Vergleich                   1=="1" => False
~       Bitweises Not               ~3-4 = -8
&       Bitweises Und
|       Bitweises Oder
^       Bitweises XOR
or      Boolean or
and     Boolean and
not     Boolean not
in      Element von                 1 in [0,1,2,]
<<      Shiftoperator
>>      Shiftoperator

"""
