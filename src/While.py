#While
i = 0

while i <= 5:
    print(i)
    i += 1

#Else
i = 5

while i >= 0 :
    print(i)
    i -= 1
else:
    print("Programm finished")